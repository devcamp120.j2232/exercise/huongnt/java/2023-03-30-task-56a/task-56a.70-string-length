package com.devcamp.stringlength;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StringLengthApplication {

	public static void main(String[] args) {
		SpringApplication.run(StringLengthApplication.class, args);
	}

}
