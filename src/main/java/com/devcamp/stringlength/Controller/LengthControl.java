package com.devcamp.stringlength.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class LengthControl {
    @GetMapping("/length")
    public int getLengthApi(@RequestParam String stringlength){
        return stringlength.length();
    }
}
